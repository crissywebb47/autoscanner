#!/usr/bin/env bash

export WORKDIR="$HOME"/autoscanner
mkdir -p "$HOME"/Results/osint
export OUTDIR="$HOME"/Results/osint
cd "$WORKDIR"

echo -e "Starting GoBuster" | pv -qL $[15+(-2 + RANDOM%5)]

sudo -S gobuster vhost -u "$IP" --follow-redirects --method=GET --useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0, GomezAgent 3.0)" --domain --wordlist "$WORKDIR"/000webhost.txt --threads=5 --delay=2500ms --output="$OUTDIR"/vhost.txt --no-error --pattern="$WORKDIR"/sortedcombined-knock-dnsrecon-fierce-reconng.txt --verbose
/usr/bin/cat "$OUTDIR"/vhost.txt >>"$OUTDIR"/subdomains.txt
echo "[+] Found $(/usr/bin/cat $OUTDIR/subdomains.txt | wc -l) so far..."
