#!/usr/bin/env bash

echo "This Software will ask for root password when needed. Do not run as root" | pv -qL $[10+(-3 + RANDOM%5)]
if [[ $USER == root ]]; then exit 1; else echo "You are not root user" | pv -qL $[10+(-3 + RANDOM%5)]; echo "You may continue" | pv -qL $[10+(-3 + RANDOM%5)];fi
echo -e \\c ${USER}:${USER} >"$WORKDIR"/.users_preference.txt
export WORKDIR="$HOME"/autoscanner
#chown --recursive --reference="$WORKDIR"/.users_preference.txt "$WORKDIR"
mkdir -p "$HOME"/Results/osint
export OUTDIR="$HOME"/Results/osint
#chown --recursive --reference="$WORKDIR"/.users_preference.txt "$OUTDIR"
cd "$WORKDIR"

echo "Enter Target IP or URL: " | pv -qL $[10+(-3 + RANDOM%5)]
read -r IP

echo "Target set as $IP" | pv -qL $[10+(-3 + RANDOM%5)]


function check_wildcard() {
	if [[ "$(dig @1.1.1.1 A,CNAME {test321123,testingforwildcard,plsdontgimmearesult}.$IP +short | wc -l)" -gt "1" ]]; then
		echo "Wildcard detected .. Existing :( " | pv -qL $[10+(-3 + RANDOM%5)]
		exit 1
	fi
};

function run_gobuster() {
	echo "[+] Running gobuster on $IP ...(this could take a while)" | pv -qL $[10+(-3 + RANDOM%5)]
	sudo -S "$HOME"/go/bin/gobuster dns -d "$IP" --wordlist "$WORKDIR"/dns-full.txt -o "$OUTDIR"/go.txt
	/usr/bin/cat "$OUTDIR"/go.txt >>"$OUTDIR"/subdomains.txt
	echo "[+] Found $(/usr/bin/cat $OUTDIR/subdomains.txt | wc -l) so far ..." | pv -qL $[10+(-3 + RANDOM%5)]
};

function check_takeover() {
	echo "[+] Analysing data with Subjack ..." | pv -qL $[10+(-3 + RANDOM%5)]
	sudo -S "$HOME"/go/bin/subjack -w "$OUTDIR"/subdomains.txt -c "$WORKDIR"/fingerprints.json -o "$OUTDIR"/takeover.txt
}

# check if DNS wildcard present
check_wildcard
check_takeover

# Check if any subdomains were previously one before running run_dnsgen, otherwise skip this step
[ -s "$OUTDIR"/subdomains.txt ] && bash "$WORKDIR"/run_dnsgen.sh

#Step 1 - MassDNS Intel Gathering
bash "$WORKDIR"/run_sub-n-massdns.sh
sleep 4

#Step 2 - Run DNS-Gen
bash "$WORKDIR"/run_dnsgen.sh
sleep 4

#Step 3 - Amass recon
bash "$WORKDIR"/run_amass.sh
sleep 4

#Step 4 - Take screenshots of domains
bash "$WORKDIR"/screen_enum.sh
sleep 4

#Step 5 - Directory enumerate all found subdomains
bash "$WORKDIR"/dir_enum.sh
sleep 4

#Step 6 - GoBuster
run_gobuster
sleep 4

#Step 7 - GoBuster VHost Enumberation 
bash "$WORKDIR"/vhost_enum.sh
sleep 4

cat "$OUTDIR"/*.txt
