#!/usr/bin/env bash

export WORKDIR="$HOME"/autoscanner
mkdir -p "$HOME"/Results/osint
export OUTDIR="$HOME"/Results/osint
export WORDLIST="$WORKDIR"/subdomains-top1million-110000.txt
cd "$WORKDIR"

touch "$OUTDIR"/subbrute.txt
touch "$OUTDIR"/massdns.txt
touch "$OUTDIR"/subdomains.txt
echo "[+] Bruteforcing $IP with MassDNS & Sub.Brute ..." | pv -qL $[10+(-3 + RANDOM%5)]
sudo -S /usr/bin/massdns ${IP} -r "$WORKDIR"/resolvers.txt -o S -w "$OUTDIR"/massdns.txt
sudo -S /usr/bin/subbrute -s "$WORKDIR"/names.txt -f "$WORKDIR"/shubs-subdomains.txt -A -v -r "$WORKDIR"/resolvers.txt -o "$OUTDIR"/subbrute.txt -t "$OUTDIR"/subdomains.txt
sort -u -o "$OUTDIR"/massdns.txt "$OUTDIR"/subbrute.txt
/usr/bin/cat "$OUTDIR"/subbrute.txt -- "$OUTDIR"/massdns.txt >>"$OUTDIR"/subdomains.txt
echo "[+] Found $(/usr/bin/cat "$OUTDIR"/subdomains.txt | wc -l) so far ..." | pv -qL $[10+(-3 + RANDOM%5)]
