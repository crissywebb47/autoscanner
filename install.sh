#!/usr/bin/env bash

#git clone https://github.com/Jguer/yay.git && cd yay && makepkg -si && cd

echo -e "Server = https://archlinux.mailtunnel.eu/'\$'\repo/os/'\$'\arch\nServer = https://arch.mirror.constant.com/'\$'\repo/os/'\$'\arch" >>/etc/pacman.d/mirrorlist

curl -O https://blackarch.org/strap.sh

echo 8bfe5a569ba7d3b055077a4e5ceada94119cccef strap.sh | sha1sum -c

chmod +x strap.sh

bash strap.sh

pacman -S --noconfirm --overwrite="*" pv python39 go python python-pip bind amass subbrute massdns eyewitness tor

bash -c 'pip install dnsgen==1.0.4 --user'

bash -c 'go install github.com/haccer/subjack@latest'

bash -c 'go install github.com/OJ/gobuster@latest'

