#!/usr/bin/env bash

export WORKDIR="$HOME"/autoscanner
mkdir -p "$HOME"/Results/osint
export OUTDIR="$HOME"/Results/osint
export WORDLIST="$WORKDIR"/subdomains-top1million-110000.txt
cd "$WORKDIR"

echo "[+] Running Amass on $IP ..." | pv -qL $[10+(-3 + RANDOM%5)]

sudo -S /usr/bin/amass enum -df "$WORKDIR"/providers-data.txt -dir "$OUTDIR" -active -brute -nf "$WORKDIR"/subdomains-top1million-110000.txt -rf "$WORKDIR"/resolvers.txt -ip -nolocaldb -noalts -scripts "$WORKDIR"/scripts -if "$WORKDIR"/providers-data.csv -o "$OUTDIR"/subdomains.txt

echo "[+] Found $(/usr/bin/cat "$OUTDIR"/subdomains.txt | wc -l) so far ..." | pv -qL $[10+(-3 + RANDOM%5)]
