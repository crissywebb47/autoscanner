#!/usr/bin/env bash

export WORKDIR="$HOME"/autoscanner
mkdir -p "$HOME"/Results/osint
export OUTDIR="$HOME"/Results/osint
export WORDLIST="$WORKDIR"/subdomains-top1million-110000.txt
cd "$WORKDIR"
echo -e \\c "$USER"
chown --recursive "$USER":"$USER" "$OUTDIR"
chmod --recursive a+rwx "$OUTDIR"

echo "[+] Bruteforcing $IP with DNS.Gen. + MassDNS ..." | pv -qL $[10+(-3 + RANDOM%5)]
echo "$IP" >>"$OUTDIR"/target.txt
sudo -S /usr/bin/dnsgen -l 7 -w "$WORKDIR"/dns-Jhaddix.txt -f "$OUTDIR"/target.txt >>"$OUTDIR"/dnsgen.txt
sudo -S /usr/bin/massdns -r "$WORKDIR"/dns-resolvers.txt -o L -w "$OUTDIR"/massdns.txt "$OUTDIR"/dnsgen.txt
sort -u -o "$OUTDIR"/massdns.txt "$OUTDIR"/dnsgen.txt
/usr/bin/cat "$OUTDIR"/dnsgen.txt -- "$OUTDIR"/massdns.txt >>"$OUTDIR"/subdomains.txt
echo "[+] Found $(/usr/bin/cat $OUTDIR/subdomains.txt | wc -l) so far ..." | pv -qL $[10+(-3 + RANDOM%5)]
