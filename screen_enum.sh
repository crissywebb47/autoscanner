#!/usr/bin/env bash

export WORKDIR="$HOME"/autoscanner
mkdir -p "$HOME"/Results/osint
export OUTDIR="$HOME"/Results/osint
export WORDLIST="$WORKDIR"/sortedcombined-knock-dnsrecon-fierce-reconng.txt
cd "$WORKDIR"

# set up the directory structure
mkdir -p "$OUTDIR"/screen/
sudo -S eyewitness --web --single "$IP" --no-dns --timeout 10 --jitter 1 --delay 5 --threads 5 --max-retries 2  -d "$OUTDIR"/screen/ --results 10 --no-prompt --user-agent "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7" --difference 75 --show-selenium --resolve --prepend-https
