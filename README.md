# **AutoScanner**

## This script is an Automated OSINT / Intelligence harvesting tool to aid in penetration testing

Installing:

  ```bash
  ./install.sh
  ```

Execution:

  ```bash
  bash master.sh
  ```

## To better understand everything that takes place I highly suggest that the end user read the scripts which will be executed. The scripts are

> Order of execution:
  
    * install.sh
    * master.sh
    * run_massdns.sh
    * run_dnsgen.sh
    * run_amass.sh
    * subdomain_enum.sh
    * screen_enum.sh
    * dir_enum.sh

## H4cK 411 7h3 7h1nG5
